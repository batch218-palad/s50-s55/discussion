import Banner from '../components/Banner';

/*import { Navigate } from 'react-router-dom';*/

export default function Error() {

	const data = {
		title: "Error 404 - Page not found.",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"
	}
	
	return (
		<Banner data={data} />
		// <>
		// <h1>Page not found</h1>
		// <h4>Go back to <a style={{ textDecoration: 'none'}} href="https://zuitt.co">homepage</a></h4> {/*used zuitt.co as sample home site*/}
		// </ >
	)
};
